# analytics-env

Analytics Environment Specification for local development.

The command below will create an environment with the correct packages and versions for development.

```
conda env create -f environment.yaml
```

To activate

```
source activate mhs-analytics
```

To deactivate

```
source deactivate
```
